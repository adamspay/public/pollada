<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\SerializerInterface;

use Twig\Environment;

use Parse\ParseClient;


abstract class Pollada 
extends AbstractController
{
    
   
    private static $parseClientInitialized=false, $adamsPayConfig = null;

    
    static protected function horaEnUTC():\DateTimeImmutable {
        return new \DateTimeImmutable('now',new \DateTimeZone('UTC'));
    }
    
    
    protected static function initBack4App() {
        if( !self::$parseClientInitialized ){
            
            self::$parseClientInitialized = true;
            $apiKey = self::envString( 'BACK4APP_API_KEY' );
            $restKey = self::envString( 'BACK4APP_REST_KEY' );
            $masterKey = self::envString( 'BACK4APP_MASTER_KEY' );
            if( !$apiKey || !$restKey || !$masterKey ){
                throw new \Exception('.env debe contener valores para BACK4APP_API_KEY, BACK4APP_REST_KEY y BACK4APP_MASTER_KEY');
            }
            ParseClient::initialize( $apiKey,$restKey, $masterKey);
            ParseClient::setServerURL('https://parseapi.back4app.com', '/');

        }
    }
    
    protected static function getAdamsPayConfig():array {
        
        if( !self::$adamsPayConfig ){
            
            $env = self::envString('ADAMSPAY_ENV');
            $apiKey = self::envString('ADAMSPAY_API_KEY');
            $apiSecret = self::envString('ADAMSPAY_API_SECRET');
            if( !$env || !$apiKey || !$apiSecret ){
                throw new \Exception('.env debe contener valores para ADAMSPAY_ENV, ADAMSPAY_API_KEY y ADAMSPAY_API_SECRET');
            }
            if( $env === 'local'){
                $apiHost = 'adams-test.host';

            }
            else
            if( $env === 'test'){
                $apiHost = 'staging.adamspay.com';
            }
            else
            if( $env === 'prod'){
                $apiHost = 'checkout.adamspay.com';

            }
            else {
                throw new \Exception('Ambiente inválido ('.$env.') para AdamsPay');
            }
            self::$adamsPayConfig = ['env'=>$env,'apiUrl'=>'https://'.$apiHost.'/api/v1','apiKey'=>$apiKey,'apiSecret'=>$apiSecret];
        }
        return self::$adamsPayConfig;        
    }
    
    protected static function envString( $name ):?string {
        return isset($_ENV[$name]) && \is_string($_ENV[$name]) ? $_ENV[$name] : null;
    }
    
    

    protected  static function base64UrlEncode($data) { 
        return \rtrim(\strtr(\base64_encode($data), '+/', '-_'), '='); 
    }
   
    public static function getSubscribedServices()
    {
        return array(
            'router' => RouterInterface::class,
            'http_kernel' => HttpKernelInterface::class,
            'twig' => Environment::class,
            'serializer' => '?'.SerializerInterface::class,
        );
    }
    
    
    

    
}
