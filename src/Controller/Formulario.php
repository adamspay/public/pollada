<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response ;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


use App\Controller\Pollada;

use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseObject;

use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

class Formulario 
extends Pollada
{
    
    const viewInicio = 'inicio'
        , viewRegistro = 'registro'
        , viewCliente = 'cliente'
        , viewPagar = 'pagar';
    
    
    // -----------------------------------------------------------------------
    // Formulario
    // Este sitio muestra solamente 1 formulario con 3 vistas:
    // 
    // Inicio: Solicita cédula para verificar estado
    // Registro: Solicita datos si la cédula no está registrada
    // Cliente: Muestra datos del registro y si está pagado o no
    // 
    
    function mostrar( Request $request ):Response {
        
  
        $ahoraEnUTC = self::horaEnUTC();
        $doc =  \mb_substr(\trim($request->get('doc')),0,16);
        $fields = ['doc'=>$doc];
        $view  =self::viewInicio;
        
        // Inicializar ambos servicios AdamsPay y Back4App
        try {
            self::initBack4App();
            $adamsPayConfig = $this->getAdamsPayConfig();
            $alert = null;
       }
        catch( \Throwable $e ){
            $alert=['type'=>'error','text'=>'Error al inicializar: '. \htmlspecialchars($e->getMessage())];
        }

        // El while() no es para loops, usamos para poder detener
        // el procesamiento en cualquier momento con un"break"
        while( !$alert ){
            
            // Es el POST de un form?
            $post = $request->getMethod() === 'POST' ? $request->request : null;
            
            if( !preg_match('/^[0-9]{3,12}$/',$doc) ){
                if( $post ){
                    // Si es post del formulario, avisar al usuario
                    $alert=['type'=>'error','text'=>'Por favor, ingresa tu cédula sin puntos o espacios separadores'];
                    break;
                }
                // Si no es post: ignorar
                $fields['doc'] = $doc = null;    // ignorar
            }
            
            if( $doc ){
                 // Buscar el cliente en nuestra DB en la nube (Back4App)
                 try {
                    $miCliente = (new ParseQuery('clientes'))
                        ->equalTo('doc',$doc)
                        ->first();


                    if( !empty($miCliente) ){
                        // Cliente ya existe
                        
                        // ObjId is generado por la DB
                        $fields['objId'] = $miCliente->getObjectId();
                        
                        // Name e Email los completamos al registrarlo
                        $fields['name'] = $miCliente->get('name');
                        $fields['email'] = $miCliente->get('email');

                        // Estos campos los carga Controller\AdamsPay::webhook al procesar una notifiación:
                        // 1) debtObjId: ObjectId de nuestra deuda
                        // 2) payTime: Hora de pago
                        $fields['debtObjId'] = $miCliente->get('debtObjId');
                        $fields['payTime'] = $miCliente->get('payTime');
                    }
                    else {
                        // Cliente no existe aún, todo NULL
                        $miCliente=  $fields['objId'] = null;
                    }
                 }
                 catch (\Exception $e) {
                    $alert=['type'=>'error','text'=>'Error al buscar el cliente: '. \htmlspecialchars($e->getMessage())];
                    $view  =self::viewInicio;
                    break;
                 }   
            } else {
                
                // Sn cliente
                $miCliente = null;
            }



            // Name e Email los completamos al registrarlo
            $clienteEstaRegistrado = $miCliente && $miCliente->get('email') && $miCliente->get('name');
            


            // Decidir la vista DEFAULT
            if( !$doc ){    // Sin Cédula, ir al inicio
                $view=self::viewInicio;
            }
            else
            if( !$clienteEstaRegistrado ){
                $view=self::viewRegistro;   // No está registrado: Registrar
            }
            else {
                $view=self::viewCliente;    // Ya está registrado: Mostrar información
            }

            if( !$post ){
               break;   // No es post de un form, terminamos acá
            }
            
            // El form que postea envía el "view"
            $postView = $post->get('view');
            if( $postView === self::viewInicio) {
                break;   // Todo lo necesario para el inicio ya fue hecho más arriba
            }


            if( $postView === self::viewRegistro )
            {
                // Post del formulario de registro
                
                if( $clienteEstaRegistrado ){
                    break;  // Ignorar porque ya está registrado
                }

                // Leer y verificar datos del registro
                $fields['name'] = $name = \mb_substr(\trim($post->get('name')), 0 , 200);
                $fields['email'] = $email = \mb_substr(strtolower(\trim($post->get('email'))), 0 , 200);

                if( strlen($name)< 4 ){
                    $alert=['type'=>'error','text'=>'El nombre es demasiado corto'];
                    break;  // detener procesamiento
                }

                if( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
                    $alert=['type'=>'error','text'=>'El email es inválido'];
                    break;  // detener procesamiento
                }

                // Si no existe en nuestra DB crear uno nuevo objeto
                if( !$miCliente ){
                    $miCliente = new ParseObject('clientes');
                    $miCliente->set('doc',$doc);
                }
                
                // Completar nuestros datos
                $miCliente->set('name',$name);
                $miCliente->set('email',$email);
                
                try {
                    $miCliente->save();
                    
                    // Listo ahora ya tenemos el cliente, mostrar su vista
                    $fields['objId'] = $miCliente->getObjectId();
                    $view = self::viewCliente;
                }                
                catch (\Exception $e) {
                    $alert=['type'=>'error','text'=>'Error al crear el cliente: '. \htmlspecialchars($e->getMessage())];
                }
                
                break;  // procesamiento completo
            }

            if( $postView === self::viewCliente )
            {
                // Post desde el formulario de cliente
                // El botón apretado se llama siempre "submit"
                $tarea = '?';
                
                try {
                    
                    $mostrarQR=false;
                    if( $post->get('submit') === 'pay' )
                    {
                        // Click en "pagar ahora"
                        $tarea = 'crear la deuda';

                        // Crear la deuda en Adams
                        $deudaAdamsPay = $this->crearDeudaEnAdamsPay($adamsPayConfig,$miCliente, 'Pollada',50000);

                        // Crear nuestro registro de pago de la pollada
                        $miDeuda = new ParseObject('deudas');

                        // Asociar al doc e ID del cliente
                        $miDeuda->set('clientDoc',$doc);
                        $miDeuda->set('clientObjId',$miCliente->getObjectId());

                        // Asociar a la deuda en AdamsPay
                        $miDeuda->set('adamsDocId',$deudaAdamsPay['docId']);
                        $miDeuda->save();

                        // Listo, mostrar el QR o el link
                        $mostrarQR = true;

                    }
                    else
                    if( $post->get('submit') === 'view' )
                    {
                        // Click en "ver pago"
                        $tarea = 'leer la deuda';
                        
                        // Obtener nuestra deuda, el ID se queda en el cliente
                        $objIdDeuda = $miCliente->get('debtObjId');

                        
                        // Leer nuestra deuda
                        $misDeudas = new ParseQuery('deudas');
                        $miDeuda = $misDeudas->get($objIdDeuda);

                        // En nuestra deuda está el ID de AdamsPay
                        $deudaAdamsPay = $this->leerDeudaDeAdamsPay($adamsPayConfig,$miDeuda->get('adamsDocId'));

                        // Listo, ir a la página de la deuda en AdamsPay
                        $mostrarQR = false;
                    }
                    else {
                        $tarea = 'procesar la tarea';
                        throw new \Exception('Acción inválida');
                    }

                    $urlDeudaAdamsPay = $deudaAdamsPay['payUrl'];

                    // A donde retornar cuando el cliente termina en AdamsPay
                    // Será procesado por Controller\AdamsPay::retorno
                    $miUrlDeRetorno = $this->generateUrl('retorno',['doc'=>$miCliente->get('doc')], UrlGeneratorInterface::ABSOLUTE_URL);

                    // Pasamos algunos datos de nuestro cliente a AdamsPay
                    // No es necesario, pero permite que pre-complete algunos campos
                    // asi que conviene hacerlo (Para VPOS2.0, es requerido)
                    // AdamsPay recibe los datos como un JWT token firmado por nuestra APP
                    $infoCliente =  $this->generarIdTokenParaAdamsPay($adamsPayConfig,$miCliente,48);

                    // Pedimos a adams retornar a la página de este mismo cliente sin agregar información del resultado
                    $miUrlPago = $urlDeudaAdamsPay 
                                    . '?return_url='.\urlencode($miUrlDeRetorno)
                                    . '&idtoken='.\urlencode($infoCliente);

                    
                    if( $mostrarQR ){
                        
                        $view = self::viewPagar;
                        $fields['urlDePago'] = $miUrlPago;
                        // La URL del QR debe ser más corta y sin información porque sino, no entra
                        $fields['qrDePago'] = $this->generarQR( $urlDeudaAdamsPay  );
                        
                    } else {
                        return $this->redirect($miUrlPago);   // Enviar cliente a la URL de la deuda en AdamsPay
                    }
                    
                } catch (\Exception $e) {
                     $alert=['type'=>'error','text'=>'Error al '.$tarea.': '.\htmlspecialchars($e->getMessage())];
                }
                 
                 
                break;  // procesamiento completo

            }

            // Si llegamos acá, no hemos manejado el caso!
            // Salir del LOOP con un error
            $alert=['type'=>'error','text'=>'Error interno: No se ha manejado el estado actual'];
            break;
        }
        
        
        if( !isset($fields['email']) && $view===self::viewRegistro ){
            // Generar mail de mentirillas
            $fields['email']='pollada@example.com';
        }

        // Render de nuestra página
        $twigVars = ['page'=>['view'=>$view,'alert'=>$alert,'fields'=>$fields]];
        
        
        return $this->render("formulario-{$view}.twig", $twigVars);

    }
    
    private function generarQR( string $url ):string {
        $qropts = new QROptions([
                'version'    => 5,
                'outputType' => QRCode::OUTPUT_IMAGE_PNG,
                'eccLevel'   => QRCode::ECC_L,
                'scale'=>8,
                'imageBase64'=> true,
                'imageTransparent'=>false,
                ]);
        
        // Usar colores de AdamsPay!
        $qropts->moduleValues = [
            // finder
            1536 => [222, 0,0], 
            6    => [255, 255, 255], 
            // alignment
            2560 => [222, 0, 0],
            10   => [255, 255, 255],
            // timing
            3072 => [222, 0, 0],
            12   => [255, 255, 255],
            // format
            3584 => [222, 0, 0],
            14   => [255, 255, 255],
            // version
            4096 => [222, 0, 0],
            16   => [255, 255, 255],
            // data
            1024 => [0, 0, 0],
            4    => [255, 255, 255],
            // darkmodule
            512  => [0, 0, 0],
            // separator
            8    => [255, 255, 255],
            // quietzone
            18   => [255, 255, 255],
        ];


        return (new QRCode($qropts ))->render($url);

    }
   
    
    //
    // AdamsPay recibe datos de usuarios con un JWT token firmado por nuestra APP y
    // con propiedades según especificaciones de open id
    // Ver: https://jwt.io y https://openid.net/specs/openid-connect-core-1_0.html#IDToken
    //
    private function generarIdTokenParaAdamsPay(array $adamsPayConfig, ParseObject $miCliente, int $horasValidez )
    {
        
        $ahora = self::horaEnUTC();
        $expira = $ahora->add(new \DateInterval('PT'.$horasValidez.'H'));
        
        // Cabecera estándar
        $header =['alg'=>'HS256','typ'=>'JWT'];
        
        $payload =['iss'=>'self'        // Nuestro usuario
                ,'iat'=>$ahora->getTimestamp()
                ,'exp'=>$expira->getTimestamp()
                ,'sub'=>null            // null: No crear registro permanente en AdamsPay
                ,'document'=>['type'=>'cip','number'=>$miCliente->get('doc')]   
                ,'email'=>$miCliente->get('email')
                ,'name'=>$miCliente->get('name')
                ];
        $b64Header = self::base64UrlEncode(\json_encode($header));
        $b64Payload = self::base64UrlEncode(\json_encode($payload));
        
        $jwtContent = $b64Header . '.' . $b64Payload;
        $signature = \hash_hmac('sha256',$jwtContent, $adamsPayConfig['apiSecret'], true);
        
        $jwtSignature = self::base64UrlEncode( $signature );
        
        return $jwtContent . '.' . $jwtSignature;

        
    }

    // Lee el documento de una deuda en AdamsPay
    private function leerDeudaDeAdamsPay( array $adamsPayConfig, string $adamsDebtId): array {

        $apiUrl = $adamsPayConfig['apiUrl'] . '/debts/'. $adamsDebtId;
        $apiKey = $adamsPayConfig['apiKey'];
        
          // Hacer el POST
        $curl = \curl_init();

        \curl_setopt_array($curl,[
             CURLOPT_URL => $apiUrl,
             CURLOPT_HTTPHEADER => ['apikey: '.$apiKey],
             CURLOPT_RETURNTRANSFER => true,
            \CURLOPT_SSL_VERIFYHOST=>false,
            \CURLOPT_SSL_VERIFYPEER=>false
         ]);
        
        $response = \curl_exec($curl);
        if( $response ){
            $data = \json_decode($response,true);
            if( isset($data['debt']) ){
                return $data['debt'];
            }
            $err = isset($data['meta']['description']) ? $data['meta']['description'] : 'Respuesta del servidor no reconocida';//  ('.\htmlspecialchars($response).')';
        }
        else {
          $err = 'curl_error: '.curl_error($curl);
        }
        curl_close($curl);

        throw new \Exception('GET '.$apiUrl.' falló. '.($err ? ' - '.$err : null));
        
    }

    // Crea un documento de una deuda en AdamsPay
    private function crearDeudaEnAdamsPay( array $adamsPayConfig, ParseObject $miCliente,string $concepto, int $valor): array {


        // idDeuda es opcional, un sistema pondría su propio ID en ella
        $idDeuda = 'pollada-1-'.$miCliente->get('doc');
        $apiUrl = $adamsPayConfig['apiUrl'] . '/debts?update_if_exists=1';
        $apiKey = $adamsPayConfig['apiKey'];

        // Hora DEBE ser en UTC!
        $ahora = new \DateTimeImmutable('now',new \DateTimeZone('UTC'));
        $expira = $ahora->add(new \DateInterval('P2D'));

        // Crear modelo de la deuda
        $deuda = [
            'docId'=>$idDeuda,
            'label'=>$concepto,
            'amount'=>['currency'=>'PYG','value'=>strval($valor)],
            'target'=>[
                'type'=>'cip'
                ,'number'=>$miCliente->get('doc')
                ,'label'=>$miCliente->get('name')
            ]
            ,'validPeriod'=>[
                'start'=>$ahora->format(\DateTime::ATOM),
                'end'=>$expira->format(\DateTime::ATOM)
            ]
        ];


        // Crear JSON para el post
        $post = \json_encode( ['debt'=>$deuda] );


        // Hacer el POST
        $curl = \curl_init();

        \curl_setopt_array($curl,[
         CURLOPT_URL => $apiUrl,
         CURLOPT_HTTPHEADER => ['apikey: '.$apiKey,'Content-Type: application/json'],
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_CUSTOMREQUEST=>'POST',
         CURLOPT_POSTFIELDS=>$post,
            \CURLOPT_SSL_VERIFYHOST=>false,
            \CURLOPT_SSL_VERIFYPEER=>false
         ]);

        $response = \curl_exec($curl);
        if( $response ){
            $data = \json_decode($response,true);
            if( isset($data['debt']) ){
                return $data['debt'];
            }
            $err = isset($data['meta']['description']) ? $data['meta']['description'] : 'Respuesta del servidor no reconocida'; // ('.\htmlspecialchars($response).')';
        }
        else {
          $err = 'curl_error: '.curl_error($curl);
        }
        curl_close($curl);

        throw new \Exception('POST '.$apiUrl.' falló. '.($err ? ' - '.$err : null));
    }
}
