<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response ;
use Symfony\Component\HttpFoundation\JsonResponse ;


use App\Controller\Pollada;

use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseObject;

class AdamsPay 
extends Pollada
{
    
    // -----------------------------------------------------------------------
    // Procesa las notificaciones de adamspay en este webhook
    // El valor de retorno debe ser:
    // Status 200: Procesado
    // Status 202: Ignorado
    // Otros non 200: Adams reintentarará
    function webhook( Request $request ):Response {
        
        try {
            
            self::initBack4App();
            $adamsPayConfig = $this->getAdamsPayConfig();
            
            if( $request->getMethod()!=='POST' || $request->headers->get('content-type') !== 'application/json' ){
                throw new \Exception('Request inválido');
            }
            $content = $request->getContent();
            $sentHash = $request->headers->get('x-adams-notify-hash');
            $myHash = \md5( 'adams' . $content . $adamsPayConfig['apiSecret'] );

            if( $sentHash !== $myHash ){
                throw new \Exception('Firma inválida');
            }

            $post = \json_decode($request->getContent(),true);
            $notify = is_array($post) && array_key_exists('notify',$post) ? $post['notify'] : null;

            if( !is_array($notify) || !array_key_exists('type', $notify)){
                throw new \Exception('Formato inválido');
            }

            if( $notify['type'] === 'debtStatus'){
                return $this->procesarEstadoDeuda( $post['debt'] );
            }
            
            // Ignorado
            $responseCode = 202;
            $responseData = ['info'=>'No procesamos esta notificación'];
        }
        catch( \Throwable $e ){
            $responseCode = 500;
            $responseData = ['info'=>'Error','exception'=>$e->getMessage(),'line'=>$e->getLine(),'trace'=>$e->getTrace()];
        }
        return new JsonResponse( $responseData, $responseCode);
    }
    
    
    // -----------------------------------------------------------------------
    // Solicitamos que los pagos en AdamsPay retornen a esta página
    // donde podemos mostrar el resultado, etc.
    // 
    // También es buena idea configurar esta página como la "URL de retorno" de 
    // nuestra Aplicación en AdamsPay.
    //
    function retorno( Request $request , $doc ):Response {
        
        // Esta página mapearía los parámetros de AdamsPay a los que usamos nosotros y mostraría el resultado.
        // Por ahora, simplemente retornamos al indice con el DOC del cliente
        
        if( !$doc ){
            // No vino con el DOC: Significa que el usuario se fue por otro camino 
            // y no con la URL que generamos desde el formulario (un QR, por ejemplo)
            // Veamos si podemos obtener el DOC de los datos que envía AdamsPay
            $doc = $this->buscarDocRelacionado($request);
        }
        return $this->redirectToRoute('index',$doc ? ['doc'=>$doc]  : [] );
    }
    
    // -----------------------------------------------------------------------
    // Actualiza el estado de nuestra deuda y cliente
    //
    private function procesarEstadoDeuda( array $deudaAdams ):JsonResponse {
        
        $adamsDocId = $deudaAdams['docId'];
        $deudaPagada = $deudaAdams['payStatus']['status'] === 'paid';
        $ahora = self::horaEnUTC()->format(\DateTime::ATOM);
        
        // Encontremos la deuda en nuestra DB
        $miDeuda = (new ParseQuery('deudas'))
                    ->equalTo('adamsDocId',$adamsDocId)
                    ->first();
        
        
        if( empty($miDeuda) ){
            return new JsonResponse( ['info'=>'Deuda no encontrada'], 202);  // Procesado pero ignorado
        }
        $objIdDeuda = $miDeuda->getObjectId();
        $objIdCliente = $miDeuda->get('clientObjId');

        $miDeuda->set('payTime',$deudaPagada ? $ahora : null);
        $miDeuda->save();

        try {
            $clientes = new ParseQuery('clientes');
            $miCliente = $clientes->get($objIdCliente);
        }
        catch( ParseException $e ) {
            $miCliente = null;
        }
        if( empty($miCliente) ){
            return new JsonResponse( ['info'=>'Cliente no encontrado'], 202);   // Procesado pero ignorado
        }
        if( $deudaPagada ){
            // Deuda pagada!
            $miCliente->set('debtObjId',$objIdDeuda);
            $miCliente->set('payTime',$ahora);
            $miCliente->save();
        }
        else 
        if( $miCliente->get('debtObjId') === $objIdDeuda){
            // Deuda no está pagada
            // Si era *LA* deuda con la que pagó el cliente, actualizar a "no pagado" 
            $miCliente->set('debtObjId',null);
            $miCliente->set('payTime',null);
            $miCliente->save();
        }
            
        return new JsonResponse( ['info'=>'procesado'], 200); // Procesado
        
    }
    
    // -----------------------------------------------------------------------
    // Intenta determinal la cédula de nuestro cliente a partir de los 
    // parámetros de retorno de AdamsPay
    //
    private function buscarDocRelacionado( Request $requestDeRetorno ):?string {
        $valoresDeRetorno = $requestDeRetorno->query;

        // ¿De donde vuelve el usuario?
        $adamsType = $valoresDeRetorno->get('type');
        if( $adamsType === 'debt' )
        {
            // Usuairo vuelve de la página de una deuda, obtengamos el ID de la deuda
            $adamsDocId = $valoresDeRetorno->get('doc_id');
            try {
                // Encontremos la deuda correspondiente en nuestra DB
                self::initBack4App();
                
                $miDeuda = (new ParseQuery('deudas'))
                            ->equalTo('adamsDocId',$adamsDocId)
                            ->first();

                if( !empty($miDeuda) ){
                    // Encontramos la deuda, en ella deberíamos haber
                    // registrado el documento del cliente:
                    return $miDeuda->get('clientDoc');
                }
            }    
            catch( \Throwable $e ){
                // Ignorar errores
            }
        }
        return null;
    }

    
}
