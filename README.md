# pollada

Ejemplo de venta de adhesiones a una *pollada* usando servicios en la nube, implementada en **Symfony** (https://symfony.com)


## Cómo funciona
El sitio muestra un  formulario para consultar el estado de una cédula. Si la cédula no está registrada, solicita registrarse y si ya está registrada muestra la opción de pagar (o datos del pago, si ya se realizó)
Los datos de registro y pago son almacenados en una base de datos en la nube

**Versión live:**  https://pollada.devzone.adamspay.com

## Controladores

### Controller\Formulario::mostrar
Muestra el formulario correspondiente:
- Inicio: Solicita cédula para verificar estado.
- Registro: Solicita datos si la cédula no está registrada.
- Cliente: Muestra datos del cliente, con la opción de pagar o ver el pago hecho.
- Pagar: Muestra el link y un QR de pago

### Controller\AdamsPay::webhook
Procesa las notificaciones de AdamsPay y actualiza la base de datos.
Retorna estado:
```200```: Procesado
```202```: Ignorado
```500```: Error

### Controller\AdamsPay::retorno
La página a la que vuelve un usuario luego de interactuar con AdamsPay.
Mapea los parámetros de AdamsPay a los nuestros y retorna a la página del formulario.

## Base de datos
 Tenemos 2 tipos de objetos en nuestra DB:
 
#### Clientes
```objectId``` ID único asignado por la DB
```doc``` Cédula
```name```  Nombre
```email``` Email
``` debtObjId``` ID de la deuda con la que se pagó
```payTime``` Copiado del payTime de la deuda pagada

#### Deudas
```objectId``` ID único asignado por la DB
```clientDoc``` Cédula del cliente
```clientObjId``` ID del cliente
```adamsDocId``` ID de la de deuda en AdamsPay
```payTime``` Fecha de pago

    

# Cómo crear tu propia instancia

#### 1. Instala las dependencias 
 1. Ejecuta ```composer install``` en la raíz  (https://getcomposer.org/download/)
 2. Crea tu cuenta y comercio en AdamsPay (https://adamspay.com)
 3. Crear tu cuenta y DB en Back4App (https://back4app.com)
 Crea 2 *collections* en la DB: ```deudas``` y ```clientes```

#### 2. Configura el archivo raíz ```.env```
1. Completa los valores de tu aplicación de AdamPay (Configurar > Aplicaciones)
```
ADAMSPAY_ENV=test
ADAMSPAY_API_KEY
ADAMSPAY_API_SECRET
```
2. Completa los valores de  tu base de datos en BACK4APP (Help > Getting Started > Initializing Parse SDK)
```
BACK4APP_API_KEY
BACK4APP_REST_KEY
BACK4APP_MASTER_KEY
```
#### 3. Levanta la página
Deberías apuntar tu servidor http al directorio ```public ```  y *lista la pollada*


Actualizado: Mayo 2020
